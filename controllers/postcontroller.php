<?php
class PostController extends Controller {
	
	public function add() {
		header('Content-type:application/json');
		try {
			if($this->getToken() != session_id()) { throw new Exception('Authentication Error. '); } 
			
			if(!$this->_sessionOwner instanceof User) { throw new Exception('Not a valid session '); }
			
			$expire = $this->hasParam('expire') && $this->getParam('expire') != null ? $this->getParam('expire') : null;
			$media = $this->hasParam('media') && $this->getParam('media') != null ? explode(',',$this->getParam('media')) : null;
			$description = $this->hasParam('description') && $this->getParam('description') != null ? $this->getParam('description') : null;
			$price = $this->hasParam('price') && $this->getParam('price') != '' ? $this->getParam('price') : null;
			$subject = $this->hasParam('subject') && $this->getParam('subject') != '' ? $this->getParam('subject') : null;
			
			if(is_null($subject)) { throw new Exception('Subject is required'); }
			if(is_null($expire)) { throw new Exception('Expire date is required'); } 
			
			$post = new Post();
			$post->setCreatedBy($this->_sessionOwner)
				->setCreatedDate(time())
				->setExpireDate(strtotime($expire))
				->setDescription($description)
				->setMedia($media)
				->setPrice($price)
				->setSubject($subject)
				->save();
			echo json_encode(array('success' => true, 'message' => 'Successfully saved the post '.$subject));
		}
		catch (Exception $e) {
			echo json_encode(array('success' => false, 'message' => $e->getMessage()));
		}
	}
	
	public function get() {
		header('Content-type:application/json');
		try { 
			$user = $this->hasParam('username') ? User::get($this->getParam('username')) : null;
			
			if(!$user instanceof User) { throw new Exception('Not a valid user'); }
			$posts = $user->getPosts();
			$encodable = array();
			foreach ($posts as $post) {
				array_push($encodable,$post->getShowable());
			}
			
			echo json_encode(array('success' => true,'message' => '', 'data' => $encodable));
		}
		catch (Exception $e) {
			
		}
	}
}