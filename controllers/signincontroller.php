<?php
use Portus\Controller;
use Gt\User;
class SigninController extends  Controller {
	
	public function index() {
		header('Content-type:application/json');
		try {
			$username = $this->hasParam('username') && $this->getParam('username') != '' ? $this->getParam('username') : null;
			$password = $this->hasParam('password') && $this->getParam('password') != '' ? $this->getParam('password') : null;
			$user = User::get($username);
			
			if(is_null($username )) { throw new Exception('Username Required'); }
			if(is_null($password )) { throw new Exception('Username Required'); }
			if(!$user instanceof User) { throw new Exception('Not a valid user'); }
			if($user->getPassword() != Factory::getHash($password)) { throw new Exception('Invalid Credentials.'); }
			Auth::setSessionOwner($user);
			
			echo json_encode(array('success' => true, 'message' => 'Successfully Logged in','auth_token' => session_id()));
			
		}
		catch (Exception $e) {
			echo json_encode(array('success' => false, 'message' => $e->getMessage()));
		}
	}
	
	public function status() {
		header('Content-type:application/json');
		echo json_encode(array('success' => $this->_sessionOwner instanceof User,));
	}
	
}
?>