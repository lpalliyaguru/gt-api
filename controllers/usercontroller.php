<?php
use Portus\Controller;
use Portus\Factory\Factory;
use Jabber\Jabber;
use Jabber\CommandJabber;
use Jabber\AddMessenger;
use Gt\User;
use Portus\Auth\Auth;
use Portus\Exception\ParameterException;

class UserController extends Controller {	
	
	public function add() {
		header('Content-type:application/json');
		try {
			$name = $this->hasParam('name') && $this->getParam('name') != null ? $this->getParam('name') : null;
			$email = $this->hasParam('email') && $this->getParam('email') != null ? $this->getParam('email') : null;
			$password = $this->hasParam('password') && $this->getParam('password') != null ? $this->getParam('password') : null;
			$facebook_id = $this->hasParam('facebook_id') && $this->getParam('facebook_id') != null ? $this->getParam('facebook_id') : null;
			$quotation = $this->hasParam('quotation') && $this->getParam('quotation') != null ? $this->getParam('quotation') : null;
			$phone = $this->hasParam('phone') && $this->getParam('phone') != null ? $this->getParam('phone') : null;
			
			if(is_null($email)) { throw new ParameterException('Email cannot be empty'); }
			if(is_null($password)) { throw new ParameterException('Password cannot be empty'); }
			$user = new User();
			$user->setName($name)
				->setEmail($email)
				->setPhone($phone)
				->setQuotation($quotation)
				->setPassword(Factory::getHash($password));
			
			$picture = $this->getFiles('picture');
			$bucket = new Portus\File\Bucket(APP_FILE_UPLOAD_PATH);

			if(!is_null($picture)) {
				$picture = (object)$picture;
				$storablePicture = new Portus\File\File($picture);
				$storablePicture->setId(Portus\File\File::getUniqueId());
				$bucket->addObject($storablePicture);
				$user->setPicture($storablePicture->getStorableName());
			}
			
			$coverPhoto = $this->getFiles('cover');

			if(!is_null($coverPhoto)) {
				$coverPhoto = (object)$coverPhoto;
				$storableCover = new Portus\File\File($coverPhoto);
				$storableCover->setId(Portus\File\File::getUniqueId());
				$bucket->addObject($storableCover);
				$user->setCoverPhoto($storableCover->getStorableName());
			}
			$bucket->flush();
			$user->save();
			Auth::setSessionOwner($user);
			echo json_encode(array('success' => true,
									'message' => 'Successfully saved user '.$user->getName(),
									'auth_token' => session_id(),
									'verified' => $user->isVerified()
							));
		}
		catch (ParameterException $e) {
			echo json_encode(array('success' => false,'message' => $e->getMessage(),'code' => $e->getCode()));
		}
		catch (Exception $e) {
			if(!is_null($picture)) {
				$bucket->removeObject($storablePicture);
			}

			if(!is_null($coverPhoto)) {
				$bucket->removeObject($storableCover);
			}
			echo json_encode(array('success' => false,'message' => $e->getMessage(),'code' => $e->getCode()));
			
		}
		
	}
	
	public function addv1() {
		header('Content-type:application/json');
		try {
			$username = $this->hasParam('username') && $this->getParam('username') != null ? $this->getParam('username') : null;
			$name = $this->hasParam('name') && $this->getParam('name') != null ? $this->getParam('name') : null;
			$sex = $this->hasParam('sex') && $this->getParam('sex') != null ? $this->getParam('sex') : null;
			$email = $this->hasParam('email') && $this->getParam('email') != null ? $this->getParam('email') : null;
			$password = $this->hasParam('password') && $this->getParam('password') != null ? $this->getParam('password') : null;
			$facebook_id = $this->hasParam('facebook_id') && $this->getParam('facebook_id') != null ? $this->getParam('facebook_id') : null;
			$facebook_cover = $this->hasParam('facebook_cover') && $this->getParam('facebook_cover') != null ? $this->getParam('facebook_cover') : null;
			$language = $this->hasParam('language') && $this->getParam('language') != null ? $this->getParam('language') : null;
			$latitude = $this->hasParam('latitude') && $this->getParam('latitude') != null ? $this->getParam('latitude') : null;
			$longitude = $this->hasParam('longitude') && $this->getParam('longitude') != null ? $this->getParam('longitude') : null;
			$type = $this->hasParam('type') && $this->getParam('type') != null ? $this->getParam('type') : null;
			$region = $this->hasParam('region') && $this->getParam('region') != null ? $this->getParam('region') : null;
			$status = $this->hasParam('status') && $this->getParam('status') != null ? $this->getParam('status') : null;
			
			if(is_null($name)) { throw new Exception('Name cannot be empty'); }
			if(is_null($username)) { throw new Exception('Username cannot be empty'); }
			if(is_null($password)) { throw new Exception('Password cannot be empty'); }
			
			$user = new User();
			$user->setUsername($username)
				->setSex($sex)
				->setName($name)
				->setEmail($email)
				->setPassword(Factory::getHash($password))
				->setFacebookId($facebook_id)
				->setFacebookCover($facebook_cover)
				->setLanguage($language)
				->setLatitude($latitude)
				->setLongitude($longitude)
				->setType($type)
				->setRegion($region)
				->setStatus($status)
				->save();
			//adding user to jabber server
			$jabberInstance = new CommandJabber(true);
			$messenger = new AddMessenger($jabberInstance,$username,$password);
			// set handlers for the events we wish to be notified about
			$jabberInstance->set_handler("connected",$messenger,"handleConnected");
			$jabberInstance->set_handler("authenticated",$messenger,"handleAuthenticated");
			//$jab->set_handler("error",$addmsg,"handleError");
			
			// connect to the Jabber server
			if ($jabberInstance->connect(JABBER_SERVER)) {
				$AddUserErrorCode = 12001;
				$result = $jabberInstance->execute(CBK_FREQ,RUN_TIME);
			}
			$jabberInstance->disconnect();
			echo json_encode(array('success' => true,'message' => 'Successfully saved user '.$user->getName()));
		}
		catch (Exception $e) {
			echo json_encode(array('success' => false,'message' => $e->getMessage(),'code' => $e->getCode()));
		}
		
	}
	
	public function update() {
		header('Content-type:application/json');
		try {
			$username = $this->hasParam('username') && $this->getParam('username') != null ? $this->getParam('username') : null;
			$name = $this->hasParam('name') && $this->getParam('name') != null ? $this->getParam('name') : null;
			$sex = $this->hasParam('sex') && $this->getParam('sex') != null ? $this->getParam('sex') : null;
			$email = $this->hasParam('email') && $this->getParam('email') != null ? $this->getParam('email') : null;
			$password = $this->hasParam('password') && $this->getParam('password') != null ? $this->getParam('password') : null;
			$facebook_id = $this->hasParam('facebook_id') && $this->getParam('facebook_id') != null ? $this->getParam('facebook_id') : null;
			$facebook_cover = $this->hasParam('facebook_cover') && $this->getParam('facebook_cover') != null ? $this->getParam('facebook_cover') : null;
			$language = $this->hasParam('language') && $this->getParam('language') != null ? $this->getParam('language') : null;
			$latitude = $this->hasParam('latitude') && $this->getParam('latitude') != null ? $this->getParam('latitude') : null;
			$longitude = $this->hasParam('longitude') && $this->getParam('longitude') != null ? $this->getParam('longitude') : null;
			$type = $this->hasParam('type') && $this->getParam('type') != null ? $this->getParam('type') : null;
			$region = $this->hasParam('region') && $this->getParam('region') != null ? $this->getParam('region') : null;
			$status = $this->hasParam('status') && $this->getParam('status') != null ? $this->getParam('status') : null;
				
			$user = $this->_sessionOwner;
			if(!$user instanceof User) { throw new Exception('Invalid session'); }
			if(is_null($name)) { throw new Exception('Name cannot be empty'); }
			if(is_null($password)) { throw new Exception('Password cannot be empty'); }
			
			if(!is_null($sex) ) { $user->setSex($sex); }
			if(!is_null($name) ) { $user->setName($name); }
			if(!is_null($email) ) { $user->setEmail($email); }
			if(!is_null($password) ) { $user->setPassword(Factory::getHash($password)); }
			if(!is_null($facebook_id) ) { $user->setFacebookId($facebook_id); }
			if(!is_null($facebook_cover) ) { $user->setFacebookCover($facebook_cover); }
			if(!is_null($language) ) { $user->setLanguage($language); }
			if(!is_null($latitude) ) { $user->setLatitude($latitude); }
			if(!is_null($longitude) ) { $user->setLongitude($longitude); }
			if(!is_null($type) ) { $user->setType($type); }
			if(!is_null($region) ) { $user->setRegion($region); }
			if(!is_null($status) ) { $user->setRegion($status); }
			$user->update();
				echo json_encode(array('success' => true,'message' => 'Successfully updated user '.$user->getName()));
		}
		catch (Exception $e) {
			echo json_encode(array('success' => false,'message' => $e->getMessage(),'code' => $e->getCode()));
		}
		
	}
	
	public function test() {
		$display_debug_info = true;
		$AddUserErrorCode = 12000;
		$UserLogin='janithkalhara'; 
		$UserPass='janithkalhara';
		$jab = new CommandJabber($display_debug_info);
		$addmsg = new AddMessenger($jab,$UserLogin,$UserPass);
		// set handlers for the events we wish to be notified about
		$jab->set_handler("connected",$addmsg,"handleConnected");
		$jab->set_handler("authenticated",$addmsg,"handleAuthenticated");
		//$jab->set_handler("error",$addmsg,"handleError");
		
		// connect to the Jabber server
		if ($jab->connect(JABBER_SERVER)) {
			$AddUserErrorCode = 12001;
			var_dump($jab->execute(CBK_FREQ,RUN_TIME));
		}
		$jab->disconnect();
	}
	
	public function test2() {
		$file = (object)$this->getFiles('image',true);
		
		$storable = new Portus\File\File($file);
		$storable->setId($storable->getUniqueId());
		$storable->validateMime();
		$bucket = new Portus\File\Bucket();
		$bucket->setPath('/home/manoj/workspace/gt/uploads/');
		$bucket->addObject($storable);
		$bucket->flush();
		echo $storable->getId();
	}

}
?>