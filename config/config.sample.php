<?php
define('APP_ROOT', '/home/manoj/workspace/gt');
define('APP_FILE_UPLOAD_PATH', '/home/manoj/workspace/gt/uploads/');
define('DB_NAME','gt');
define('DB_HOST','localhost');
define('DB_USER','root');
define('DB_PASSWORD','1234');

define('DB_MONGO_NAME','gt');
define('DB_MONGO_HOST','localhost');
define('DB_MONGO_PORT','27017');
define('DB_MONGO_USER','root');
define('DB_MONGO_PASSWORD','1234');

define('JABBER_SERVER','gtjabber.net');
define('JABBER_USERNAME','gt-admin');
define('JABBER_PASSWORD','1234');
define('RUN_TIME',5);	// set a maximum run time of 5 seconds
define('CBK_FREQ',1);	// fire a callback event every second
define('CLASS_JABBER_VERSION','0.9rc1');
define('DEFAULT_CONNECT_TIMEOUT',15);
define('DEFAULT_RESOURCE','JabberClass');
define('MIN_CALLBACK_FREQ',1);
define('MAX_CALLBACK_FREQ',10);
?>