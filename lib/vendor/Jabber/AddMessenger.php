<?php
namespace Jabber;

class AddMessenger {

	function __construct(&$jab,$name,$pass) {
		$this->jab = &$jab;
		$this->jab->NewUserName = $name;
		$this->jab->NewUserPass = $pass;
	}

	// called when a connection to the Jabber server is established
	function handleConnected() {
		global $AddUserErrorCode;
		$AddUserErrorCode = 12002;
		// now that we're connected, tell the Jabber class to login
		$this->jab->login(JABBER_USERNAME,JABBER_PASSWORD);

	}

	// called after a login to indicate the the login was successful
	function handleAuthenticated() {
		global $AddUserErrorCode;
		$AddUserErrorCode = 12003;
		$this->jab->adduser_init();
	}
}
