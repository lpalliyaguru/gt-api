<?php 
namespace Jabber;
use Jabber\Jabber;

class CommandJabber extends Jabber {
	var $AddUserDialogID = 0;
	var $NewUserName, $NewUserPass;

	function adduser_init() {
		$this->AddUserDialogID = $this->_unique_id('adduserproc');
		$this->_set_iq_handler('_on_adduser_initanswer',$this->AddUserDialogID);
		$xml = '<iq from="'.($this->jid).'" id="'.$this->AddUserDialogID.'" to="'.($this->_server_host).'" type="set">
				<command xmlns="http://jabber.org/protocol/commands" action="execute" node="http://jabber.org/protocol/admin#add-user"/></iq>';
		return $this->_send($xml);
	}
	
	function _on_adduser_initanswer(&$packet) {
		global $AddUserErrorCode;
		$AddUserErrorCode = 12004;
		
		if ($this->_node($packet,array('iq','@','type'))=='result') {
			$AddUserErrorCode = 12005;
			$sessionid = $this->_node($packet,array('iq','#','command','0','@','sessionid'));
			
			if (strlen($sessionid) && $this->_node($packet,array('iq','#','command','0','@','status'))=='executing') {
  				$AddUserErrorCode = 12006;
  				$xml = '<iq from="'.($this->jid).'" id="'.$this->AddUserDialogID.'" to="'.($this->_server_host).'" type="set"><command xmlns="http://jabber.org/protocol/commands" node="http://jabber.org/protocol/admin#add-user" sessionid="'.$sessionid.'"><x xmlns="jabber:x:data" type="submit">';
  				$fieldsnode = $this->_node($packet,array('iq','#','command','0','#','x','0','#','field'));
  				$i = 0;
				do {
					$field_type = $this->_node($fieldsnode,array($i,'@','type'));
					$field_var = $this->_node($fieldsnode,array($i,'@','var'));
					$field_value = $this->_node($fieldsnode,array($i,'#','value','0','#'));
					
					if ($field_type == 'hidden') $xml .= '<field type="hidden" var="'.$field_var.'"><value>'.$field_value.'</value></field>';
					if ($field_var == 'accountjid') $xml .= '<field type="'.$field_type.'" var="accountjid"><value>'.$this->NewUserName.'@'.$this->_server_host.'</value></field>';
					if ($field_var == 'password') $xml .= '<field type="'.$field_type.'" var="password"><value>'.$this->NewUserPass.'</value></field>';
					if ($field_var == 'password-verify') $xml .= '<field type="'.$field_type.'" var="password-verify"><value>'.$this->NewUserPass.'</value></field>';
					$i++;
				}
			  	while (strlen(trim($field_type)) && $i<20);
				$xml .= '</x></command></iq>';
				$this->_set_iq_handler('_on_adduser_getresult',$this->AddUserDialogID);
				$this->_send($xml);
			}
		}
	}

	function _on_adduser_getresult(&$packet) {
		global $AddUserErrorCode;
		$AddUserErrorCode = 12007;
		
		if ($this->_node($packet,array('iq','@','type')) == 'result') {
			if ($this->_node($packet,array('iq','#','command','0','@','status'))=='completed');
			$AddUserErrorCode = 0;
		}
		$this->terminated = true;
	}

	/* following functions - for fill Vcard only */
	function addvcard_request($GivenName, $FamilyName, $MiddleName) {
		$DialogID = $this->_unique_id('addvcard');
		$this->_set_iq_handler('_on_addvcard_reply',$DialogID);
		$xml = '<iq from="'.($this->jid).'" id="'.$DialogID.'" type="set">
				<vCard xmlns="vcard-temp">
				<N><FAMILY>'.$FamilyName.'</FAMILY><GIVEN>'.$GivenName.'</GIVEN><MIDDLE>'.$MiddleName.'</MIDDLE></N></vCard></iq>';
		return $this->_send($xml);
	}

	function _on_addvcard_reply(&$packet) {
		global $AddVcardErrorCode;
		$AddVcardErrorCode = 14004;

		if ($this->_node($packet,array('iq','@','type')) == 'result') { $AddVcardErrorCode = 0; } 
		$this->terminated = true;
	}
}