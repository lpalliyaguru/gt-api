<?php
namespace Gt;

class Post {
	private $_data; 
	public function __construct($data=null) {
		if(!is_null($data)){
			$this->_data = $data;
		}		
	}
	
	public function getId() {
		return $this->_data['id'];
	}
	
	public function setId($id) {
		$this->_data['id'] = $id;
		return $this;
	}
	
	public function getCreatedBy() {
		return User::get($this->_data['created_by']);
	}
	
	public function setCreatedBy(User $user) {
		$this->_data['created_by'] = $user->getUsername();
		return $this;
	}
	
	public function getCreatedDate() {
		return $this->_data['created_date'];
	}
	
	public function setCreatedDate($date) {
		$this->_data['created_date'] = $date;
		return $this;
	}
	
	public function getExpireDate() {
		return $this->_data['expire'];
	}
	
	public function setExpireDate($date) {
		$this->_data['expire'] = $date;
		return $this;
	}
	
	public function getMedia() {
		return explode(',',$this->_data['media_array']);
	}
	
	public function setMedia($media) {
		$this->_data['media_array'] = implode(',',$media);
		return $this;
	}
	
	public function getDescription() {
		return @unserialize($this->_data['description']);
	}
	
	public function setDescription($desc) {
		$this->_data['description'] = $desc;
		return $this;
	}

	public function getPrice() {
		return $this->_data['price'];
	}
	
	public function setPrice($price) {
		$this->_data['price'] = $price;
		return $this;
	}
	
	public function getSubject() {
		return $this->_data['subject'];
	}
	
	public function setSubject($subject) {
		$this->_data['subject'] = $subject;
		return $this;
	}
	
	public function save() {
		$insert = 'INSERT INTO gt_post (`created_by`,`created_date`,`expire`,`media_array`,`description`,`price`,`subject`) VALUES (';
		$insert .= '"'.$this->_data['created_by'].'","'.$this->_data['created_date'].'","'.$this->_data['expire'].'","'.$this->_data['media_array'].'","'.$this->_data['description'].'","'.$this->_data['price'].'","'.$this->_data['subject'].'"';
		$insert .= ');';
		$db = Db::getInstance();
	
		if($db->query($insert)) {
			return true;
		}
		else {
			error_log($db->getError());
			throw new Exception('Unable to save post.');
		}
	}
	
	public static function getUserPosts(User $user) {
		$db = Db::getInstance();
		$db->query('SELECT * from gt_post WHERE created_by="'.$user->getUsername().'"');
		$result = $db->getResult();
		$posts = array();
		if($result) {
			foreach ($result as $post ) {
				array_push($posts, new Post($post));
			}
		}
		return $posts;
	}
	
	public function getShowable() {
		return array('by' => $this->getCreatedBy()->getUserName(),
					'created_date' => date('Y-m-d h:i:s',$this->getCreatedDate()),
					'expire_date' => date('Y-m-d h:i:s',$this->getExpireDate()),
					'subject' => $this->getSubject(),
					'description' => $this->getDescription(),
					'price' => $this->getPrice(),
					'media' => $this->getMedia()

		);
	}
	
}