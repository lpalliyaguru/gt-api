<?php
namespace Gt;
use Portus\Db\Db;
use Portus\Factory\Factory;

class User {
	
	private $id;
	private $name;
	private $email;
	private $phone;
	private $password;
	private $facebookId;
	private $picture;
	private $coverPhoto;
	private $contacts;
	private $chatImage;
	private $quotation;
	private $verified=false;

	public function __construct($data=null) {
		if(!is_null($data)) {
			$this->id = (string)$data->_id;
			$this->name = $data->name;
			$this->email = $data->email;
			$this->phone = $data->phone;
			$this->facebookId = $data->facebookId;
			$this->picture = $data->picture;
			$this->coverPhoto = $data->coverPhoto;
			$this->contacts = $data->contacts;
			$this->chatImage = $data->chatImage;
		}
	}
	
	public function getId() {
		return $this->id;
	}
	
	public function setId($id) {
		$this->id = $id;
		return $this;
	}
	
	public function getName() {
		return $this->name;
	}
	
	public function setName($name) {
		$this->name = $name;
		return $this;
	}
	
	public function getEmail() {
		return $this->email;
	}
	
	public function setEmail($email) {
		if(!filter_var($email)) { throw new \Exception('Not a valid email'); }
		$this->email = $email;
		return $this;
	}
	
	public function getPhone() {
		return $this->phone;
	}
	
	public function setPhone($phone) {
		$this->phone = $phone;
		return $this;
	}
	
	public function getFacebookId() {
		return $this->facebookId;
	}
	
	public function setFacebookId($facebookId) {
		$this->facebookId = $facebookId;
		return $this;
	}
	
	public function getPicture() {
		return $this->picture;
	}
	
	public function setPicture($picture) {
		$this->picture = $picture;
		return $this;
	}
	
	public function getCoverPhoto() {
		return $this->coverPhoto;
	}
	
	public function setCoverPhoto($coverPhoto) {
		$this->coverPhoto = $coverPhoto;
		return $this;
	}
	
	public function getContact() {
		if((bool)func_num_args()) {
			$contact = array_search(func_get_arg(0), $this->contacts);
			return User::get($contact);
		}
		
		return $this->contacts;
	}
	
	public function setContact($user) {
		$this->contacts = is_array($this->contacts) ? : array();
		$this->contacts[] = $user->getId();
		return $this;
	}
	
	public function getChatImage() {
		return $this->chatImage;
	}
	
	public function setChatImage($chatImage) {
		$this->chatImage = $chatImage;
		return $this;
	}
	
	public function getQuotation() {
		return $this->quotation;
	}
	
	public function setQuotation($quote) {
		$this->quotation = $quote;
		return $this;
	}
	
	public function getPassword() {
		return $this->password;
	}
	
	public function setPassword($password) {
		$this->password = $password;
		return $this;
	}
	
	public function isVerified() {
		if((bool)func_num_args()) {
			$this->verified = func_get_arg(0);
			return $this;
		}
		return $this->verified ? true : false;
	}
	

	public function save() {
		$persistable = array();
		
		if(is_null($this->id)) { $this->id = new \MongoId(); }
		$persistable = array(
				'name' => $this->name,
				'email' => $this->email,
				'phone' => $this->phone,
				'facebookId' => $this->facebookId,
				'picture' => $this->picture,
				'coverPhoto' => $this->coverPhoto,
				'chatImage' => $this->chatImage,
				'contacts' => $this->contacts,
				'varified' => $this->verified
		);
		
		$db = Db::getMongoInstance();
		
		try {
			if($db->users->update(array('_id' => $this->id), array('$set' => $persistable), array('upsert' => true))) {
				return true;
			}
			else  {
				throw new \Exception('Unable to save user '.$this->name);
			}	
		}
		catch (\Exception $e) {
			switch ($e->getCode()) {
				case 11000 : 
					throw new \Exception('Unable to save user "'.$this->name.'". User exists.');
					break;
				default:
					throw $e;
			}
		}
	}
	
	public static function get($id) {
		$ds = Db::getMongoInstance();
		$data = $ds->users->findOne(array('_id' => new \MongoId($id)));
		
		if(!is_null($data)) {
			return new User((object)$data);
		}
		return null;
	} 
}
?>
