<?php
namespace Gt;
class UserV2{
	const TYPE_NORMAL = 'NORMAL';
	const TYPE_PRIVILEGED_MERCHANT = 'PRIVILEGED_MERCHANT';
	const TYPE_NOT_PRIVILEGED_MERCHANT = 'NOT_PRIVILEGED_MERCHANT';
	
	private $_data = null;
	
	public function __construct($data=null) {
		if(!is_null($data)) {
			$this->_data = $data;
		}
	}
	
	public function getUsername() {
		return $this->_data['username'];
	}
	
	
	public function setUsername($username) {
		$this->_data['username'] = $username;
		return $this;
	}
	
	public function getName() {
		return $this->_data['name'];
	}
	
	public function setName($name) {
		$this->_data['name'] = $name;
		return $this;
	}
	
	public function getSex() {
		return $this->_data['sex'];
	}
	
	public function setSex($sex) {
		if(in_array($sex,array('MALE','FEMALE','OTHER'))) {
			$this->_data['sex'] = $sex;
		}
		else {
			throw new Exception('Invalid sex type'); 
		}
		return $this;
	}
	
	public function getEmail() {
		return $this->_data['email'];
	}
	
	public function setEmail($email) {
		$this->_data['email'] = $email;
		return $this;
	}
	
	public function getPassword() {
		return $this->_data['password'];
	}
	
	public function setPassword($password) {
		$this->_data['password'] = $password;
		return $this;
	}
	
	public function getFacebookId() {
		return $this->_data['fb_id'];	
	}	
	
	public function setFacebookId($fbid) {
		$this->_data['fb_id'] = $fbid;
		return $this;
	}
	
	public function getFacebookCover() {
		return $this->_data['fb_cover'];
	}
	
	public function setFacebookCover($cover) {
		$this->_data['fb_cover'] = $cover;
		return $this;
	}
	
	public function getLanguage() {
		return $this->_data['language'];
	}
	
	public function setLanguage($lang) {
		$this->_data['language'] = $lang;
		return $this;
	}
	
	public function getLatitude() {
		return $this->_data['latitude'];
	}
	
	public function setLatitude($lat) {
		$this->_data['latitude'] = $lat;
		return $this;
	}
	
	public function getLongitude() {
		return $this->_data['longitude'];
	}
	
	public function setLongitude($long) {
		$this->_data['longitude'] = $long;
		return $this;
	}
	
	public function setType($type) {
		if(in_array($type, array(self::TYPE_NORMAL,self::TYPE_NOT_PRIVILEGED_MERCHANT,self::TYPE_PRIVILEGED_MERCHANT))) {
			$this->_data['type'] = $type;
			return $this;
		}
		else throw new Exception('Invalid user type');
	}

	public function getRegion() {
		return $this->_data['region'];
	}
	
	public function setRegion($region) {
		$this->_data['region'] = $region;
		return $this;
	}
	
	public function getStatus() {
		return $this->_data['status'];
	}
	
	public function setStatus($status) {
		$this->_data['status'] = $status;
		return $this;
	}
	
	public function save() {
		$insert = 'INSERT INTO gt_user (`username`,`name`,`sex`,`email`,`password`,`fb_id`,`fb_cover`,`language`,`latitude`,`longitude`,`type`,`region`,`status`) VALUES (';
		$insert .= '"'.$this->_data['username'].'","'.$this->_data['name'].'","'.$this->_data['sex'].'","'.$this->_data['email'].'","'.$this->_data['password'].'","'.$this->_data['fb_id'].'","'.$this->_data['fb_cover'].'",';
		$insert .= '"'.$this->_data['language'].'","'.$this->_data['latitude'].'","'.$this->_data['longitude'].'","'.$this->_data['type'].'","'.$this->_data['region'].'","'.$this->_data['status'].'"';
		$insert .= ');';
		$db = Db::getInstance();
		
		if($db->query($insert)) {
			return true;
		}
		else {
			error_log($db->getError());
			throw new Exception('Unable to save user.');
		}
	}
	
	public function update() {
		$update = 'UPDATE gt_user SET `name`="'.$this->_data['name'].'",`sex`="'.$this->_data['sex'].'",`email`="'.$this->_data['email'].'",`password`="'.$this->_data['password'].'",`fb_id`="'.$this->_data['fb_id'].'",`fb_cover`="'.$this->_data['fb_cover'].'",';
		$update .= '`language`="'.$this->_data['language'].'",`latitude`='.$this->_data['latitude'].',`longitude`='.$this->_data['longitude'].',`type`="'.$this->_data['type'].'",`region`="'.$this->_data['region'].'",`status`="'.$this->_data['status'].'" ';
		$update .= 'WHERE username="'.$this->_data['username'].'"';
		$db = Db::getInstance();
	
		if($db->query($update)) {
			return true;
		}
		else {
			error_log($db->getError());
			throw new Exception('Unable to update user.');
		}
	}
	public static function get($username) {
		$db = Db::getInstance();
		$db->query('SELECT * FROM gt_user WHERE username="'.$username.'"');
		$result = $db->getResult();
		$user_data = $result ? array_shift($result) : null;
		return !is_null($user_data) ? new User($user_data) : null;
		
	}
	
	public function getPosts() {
		return Post::getUserPosts($this);
	}
	
}
?>