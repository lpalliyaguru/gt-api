<?php
namespace Portus\Request;

class Request {
	
	private $_get = null;
	private $_post = null;
	private $_files = null;
	private $_is_ajax = null;
	private $_uri = null;
	
	public function __construct() {
		$this->bake();	
	}
	
	public function bake() {
		$this->_get = (object)array_map(array($this,"cleanMore"), $_GET);
		$this->_post = (object)array_map(array($this,"cleanMore"), $_POST);
		$this->_files = (object)$_FILES;
		$this->_is_ajax = (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == "XMLHttpRequest") ? true : false;
		$this->_uri = $_SERVER['REQUEST_URI'];
	}
	
	public  function hasParam($key) {
		return (property_exists($this->_get, $key) || property_exists($this->_post,$key)) ? true : false;
	}
	
	public  function getParam($key) {
		if(property_exists($this->_get, $key)) { return $this->_get->$key; }
		else if(property_exists($this->_post,$key)) { return $this->_post->$key; }
		else { return false; }
	}

	public function getURI() {
		return $this->_uri;
	}
	
	public function getFiles() {
		if((bool)func_num_args()) {
			if(property_exists($this->_files, func_get_arg(0))) {
				$file = func_get_arg(0);
				return $this->_files->$file;
			}
			else {
				if(func_num_args() == 2 && func_get_arg(1) === true) {
					throw new \Exception('Cannot find file '.func_get_arg(0));
				}
				else {
					return null;
				}
			}
		}
		
		return $this->_files;
	}
	
	public function isAjax() {
		return $this->_is_ajax;
	}
	
	public function cleanMore($value){
		$value = is_array($value) ? array_map(array($this,'cleanMore'),$value) : $this->clean($value);
		return $value;
	}
	
	public function clean($value) {
		return $value;
	}
}
?>