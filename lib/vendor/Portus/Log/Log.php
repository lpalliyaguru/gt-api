<?php 
namespace Portus;

class Log {

	public static function write($message) {
		$loggable =  self::format_message($message);
		error_log('LOG :: '.$loggable);
	}
	
	public static function format_message($message) {
		if(is_object($message) || is_array($message)) {
			ob_start();
			var_dump($message);
			$buffer_content = ob_get_contents();
			ob_end_clean();
			return $buffer_content;
		}
		if(is_scalar($message)) {
			return $message;
		}
	}
}
?>