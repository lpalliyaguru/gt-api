<?php
namespace Portus;

use Portus\Auth\Auth;

class Controller {
	/**
	 * 
	 * @var $_request Portus\Request\Request
	 */
	protected $_request = null;
	protected $_sessionOwner = null;
	
	public function __construct($request=null) {
		if(!is_null($request)) {
			$this->_request = $request;
		}
		$this->_sessionOwner = Auth::getSessionOwner();
	}

	public function getParam($key) {
		return $this->_request->getParam($key);
	}
	
	public function hasParam($key) {
		return $this->_request->hasParam($key);
	}
	
	public function getFiles() {
		if((bool)func_num_args()) {
			return $this->_request->getFiles(func_get_arg(0));
		}
		return $this->_request->getFiles();
	}
	
	protected function getToken() {
		return Auth::getAuthToken();
	}
	
}

?>