<?php
namespace Portus\Auth;
use Gt\User;
class Auth {
	public static function setSessionOwner(User $user) {
		$_SESSION['user'] = array('email' => $user->getEmail(),
									'id' => (string)$user->getId(),
									'name' => $user->getName(),
									'logged_in' => time(),
								);
		return true;
	}
	
	public static function getSessionOwner() {
		return isset($_SESSION['user']) ? User::get($_SESSION['user']['id']) : null;
	}
	
	public static function getAuthToken() {
		$headers = getallheaders();
		if(isset($headers['GT-Auth-Token'])) {
			return $headers['GT-Auth-Token'];
		}
		throw new Exception('Authentication token not found');
	}
	
}