<?php
namespace Portus\File;

class Mimes {
	
	private static $mimes = array('application/pdf' => 'pdf',
							'image/jpeg' => 'jpg',
							'text/plain' => 'txt',
							'image/bmp' => 'bmp',
							'image/png' => 'png'
							);
	
	public static function getMimes() {
		return self::$mimes;
	}
	
	public static function getFileExtensionByMime($mime) {
		if(array_key_exists($mime, self::$mimes)) {
			return self::$mimes[$mime];
		}
		return null;
	}
	
}