<?php
namespace Portus\File;

interface iFile {
	public function store($path);
	public function getStorableName();	
}
?>