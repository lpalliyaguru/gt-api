<?php
namespace Portus\File;

class Bucket {
	
	private $path;
	
	private $objects = array();
	
	public function __construct($path=null) {
		if(!is_null($path)) {
			if(!is_writable($path)) { throw new \Exception('Unable to write to the directory'); }
			$this->path = $path;
		}
	}
	
	public function addObject($object) {
		$this->objects[] = $object;
	}
	
	public function flush() {
		if(is_null($this->path)) { throw new \Exception('Please set the bucket path first'); }
		foreach ($this->objects as $storable) {
			$storable->store($this->getPath());
		}
	}
	
	public function getPath() {
		return $this->path;
	}

	public function setPath($path) {
		$this->path = $path;
		return $this;
	}
	
	public function removeObject($object) {
		@unlink($this->getPath().$object->getStorableName());
	}
}
?>