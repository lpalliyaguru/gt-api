<?php
namespace Portus\File;

use Portus\File\FileException;
use Portus\File\Mimes;

class File implements iFile {
	private $name;
	private $id;
	private $size;
	private $mime;
	private $tempName;
	private $extension;
	
	public function __construct($fileData = null) {
		
		if(!is_null($fileData)) {
			$this->name = $fileData->name;
			$this->size = $fileData->size;
			$this->tempName = $fileData->tmp_name;
			$this->mime = $fileData->type;
		}
	}
	
	public function getName() {
		return $this->name;	
	}
	
	public function setName($name) {
		$this->name = $name;
		return $this;
	}
	
	public function getExtension() {
		return Mimes::getFileExtensionByMime($this->getMime());
	}
	
	public function setExtension($ext) {
		$this->extension = $ext;
		return $this;
	}
	
	public function getTempName() {
		return $this->tempName;
	}
	
	public function setTempName($tempName) {
		$this->tempName = $tempName; 
		return $this;
	}
	
	public function getId() {
		return $this->id;
	}
	
	public function setId($id) {
		$this->id = $id;
		return $this;
	}
	
	public function getSize() {
		return $this->size;
	}
	
	public function setSize($size) {
		$this->size = $size;
		return $this;
	}
	
	public function getMime() {
		return $this->mime;
	}
	
	public function setMime($mime) {
		$this->mime = $mime;
		return $this;
	}
	
	public function getStorableName() {
		return $this->getId().'.'.$this->getExtension();
	}
	
	public function store($path) {
		if(is_null($this->id)) { throw new \Exception('File id cannot be empty.'); }
		$uniqueFileName = $this->getStorableName();

		if(copy($this->getTempName(), $path.$uniqueFileName)){
			return true;	
		}
		
		throw new FileException('Unable to save file');
	}
	
	public function validateMime() {
		
		if(!array_key_exists($this->mime, Mimes::getMimes())) { throw new \Exception('Not a valid MIME type.'); }	
	}
	
	public static function getUniqueId() {
		return (dechex(rand(1,9999)) . '-' . rand(10000,99999) . '-' . dechex(rand(1,9999)) . '-' . dechex(time()));
	}
}
?>