<?php
use Portus\Request\Request;

try {
	$path = isset($_GET['url'])  && strlen($_GET['url']) != 0 ? $_GET['url'] : '/';
	$parts = explode('/', trim($path,'/'));
	$ctrl = isset($parts[0]) && strlen($parts[0]) > 0 ? $parts[0] : 'default';
	$ctrl = ucfirst($ctrl).'Controller';
	
	if(!class_exists($ctrl)) { throw new Exception('Class '.$ctrl.' cannot be found.'); }
	$action =  isset($parts[1]) ? $parts[1] : 'index';
	$request = new Request();
	$controller = new $ctrl($request);
	if(!is_callable(array($controller,$action))) { throw new Exception('Unable to call method.'); }
	$controller->$action();
	
}
catch (Exception $e) {
	echo 'Exception '.$e->getMessage();
}

function __autoload($className) {
	$classFile = strtolower($className).'.class.php';

	if(file_exists(APP_ROOT.'/lib/'.$classFile)) {
		require_once APP_ROOT.'/lib/'.$classFile;
		return true;
	}
	
	$controller = strtolower($className).'.php';

	if(file_exists(APP_ROOT.'/controllers/'.$controller)) {
		require_once APP_ROOT.'/controllers/'.$controller;
		return true;
	}
	
	if(preg_match("/\\//", $className) !== false) {
		$classFile = APP_ROOT.'/lib/vendor/';
		$file = $className;
		$file = str_replace('\\', '/', $file);
		$file = $file.'.php';
		$classFile .= $file;

		if(file_exists($classFile)) {
			require_once $classFile;
			return true;
		}
	}
	return false;
	//throw new Exception('Cannot find the class '.$className);
	
}
?>